#!/bin/bash
mkdir src
catkin config -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_INCLUDE_DIR=/usr/include/python3.6m -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.6m.so
catkin config --install

cd src
git clone -b melodic https://github.com/ros-perception/vision_opencv.git
cd ..
catkin build cv_bridge
